﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProccess
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pctLoad = New System.Windows.Forms.PictureBox()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.pctProccess = New System.Windows.Forms.PictureBox()
        Me.txtBinary = New System.Windows.Forms.TextBox()
        Me.txtResult = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.pctLoad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctProccess, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pctLoad
        '
        Me.pctLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pctLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctLoad.Location = New System.Drawing.Point(12, 12)
        Me.pctLoad.Name = "pctLoad"
        Me.pctLoad.Size = New System.Drawing.Size(128, 128)
        Me.pctLoad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pctLoad.TabIndex = 0
        Me.pctLoad.TabStop = False
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(146, 223)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(75, 23)
        Me.btnLoad.TabIndex = 1
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(146, 252)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&Move"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(146, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 46)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&Proccess"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'pctProccess
        '
        Me.pctProccess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pctProccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctProccess.Location = New System.Drawing.Point(12, 147)
        Me.pctProccess.Name = "pctProccess"
        Me.pctProccess.Size = New System.Drawing.Size(128, 128)
        Me.pctProccess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pctProccess.TabIndex = 0
        Me.pctProccess.TabStop = False
        '
        'txtBinary
        '
        Me.txtBinary.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBinary.Location = New System.Drawing.Point(227, 12)
        Me.txtBinary.Multiline = True
        Me.txtBinary.Name = "txtBinary"
        Me.txtBinary.ReadOnly = True
        Me.txtBinary.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtBinary.Size = New System.Drawing.Size(312, 128)
        Me.txtBinary.TabIndex = 2
        '
        'txtResult
        '
        Me.txtResult.Location = New System.Drawing.Point(227, 147)
        Me.txtResult.Multiline = True
        Me.txtResult.Name = "txtResult"
        Me.txtResult.ReadOnly = True
        Me.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtResult.Size = New System.Drawing.Size(312, 128)
        Me.txtResult.TabIndex = 2
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(146, 64)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 39)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "XOR Fragments"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(146, 109)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 39)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "AND Fragments"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(146, 154)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 39)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&Invert"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'frmProccess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(550, 282)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.txtResult)
        Me.Controls.Add(Me.txtBinary)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.pctProccess)
        Me.Controls.Add(Me.pctLoad)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProccess"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Image Proccessor"
        CType(Me.pctLoad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctProccess, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pctLoad As PictureBox
    Friend WithEvents btnLoad As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents pctProccess As PictureBox
    Friend WithEvents txtBinary As TextBox
    Friend WithEvents txtResult As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
End Class
