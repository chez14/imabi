﻿Imports System.IO

Public Class frmProccess
    Private Sub frmProccess_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmGeser.Show()
        frmGeser.Left = Me.Left + Me.Width
        frmGeser.Top = Me.Top

    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Dim Opener As New OpenFileDialog()
        Dim Lokasi As String = ""
        With Opener
            .Title = "Load image"
            .Filter = "Lossless Image File (*.bmp)|*.bmp"
            .ShowDialog(Me)
            If (.FileName.Length > 0 And .CheckFileExists) Then
                Lokasi = .FileName
            Else
                MsgBox("Error, unable to open file.")
                Exit Sub
            End If
        End With
        Dim Gambar As Image = Image.FromStream(New FileStream(Lokasi, FileMode.Open, FileAccess.Read))
        pctLoad.Image = Gambar
        Button1_Click(Nothing, Nothing)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim bits As Bitmap = pctProccess.Image
        Try
            If bits.Height = 0 Or bits.Width = 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        txtBinary.Text = ""
        For i = 0 To bits.Height - 1
            For j = 0 To bits.Width - 1
                If (j Mod 4 = 0 And j <> 0) Then
                    txtBinary.AppendText(" ")
                End If
                If (bits.GetPixel(j, i).R >= Byte.MaxValue / 2 And bits.GetPixel(j, i).G >= Byte.MaxValue / 2 And bits.GetPixel(j, i).B >= Byte.MaxValue / 2) Then
                    txtBinary.AppendText("1")
                Else
                    txtBinary.AppendText("0")
                End If
            Next
            txtBinary.AppendText(vbCrLf)
            txtResult.Text = BinaryToString(txtBinary.Text)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        pctProccess.Image = pctLoad.Image
    End Sub


    Public Function BinaryToString(ByVal Binary As String) As String
        Dim Characters As String = System.Text.RegularExpressions.Regex.Replace(Binary, "[^01]", "")
        Dim ByteArray((Characters.Length / 8) - 1) As Byte
        For Index As Integer = 0 To ByteArray.Length - 1
            ByteArray(Index) = Convert.ToByte(Characters.Substring(Index * 8, 8), 2)
        Next
        Return ByteToASCII(ByteArray)
        'Return System.Text.ASCIIEncoding.ASCII.GetString(ByteArray)
    End Function

    Public Function ByteToASCII(ByVal Bytenya As Byte()) As String
        Dim temp As String = ""
        For Each bet As Byte In Bytenya
            If bet = 0 Then
                Continue For
            End If
            temp += Chr(bet)
        Next
        Return temp
    End Function

    Private Sub frmProccess_Move(sender As Object, e As EventArgs) Handles Me.Move
        frmGeser.Left = Me.Left + Me.Width
        frmGeser.Top = Me.Top
    End Sub

    Private CropSite As Rectangle = New Rectangle(0, 0, 8, 8)
    Public Sub setCrop(x As Integer, y As Integer)
        CropSite.X += x
        If (CropSite.X < 0) Then
            CropSite.X = 0
        End If
        CropSite.Y += y
        If (CropSite.Y < 0) Then
            CropSite.Y = 0
        End If
        doUpdate()
    End Sub
    Public Sub setSize(size As Integer)
        CropSite.Width = size
        CropSite.Height = size
        doUpdate()
    End Sub

    Private Sub doUpdate()
        If pctLoad.Image.Height < CropSite.Height + CropSite.Y Then
            CropSite.Y = pctLoad.Image.Height - CropSite.Height
        End If
        If pctLoad.Image.Width < CropSite.Width + CropSite.X Then
            CropSite.X = pctLoad.Image.Width - CropSite.Width
        End If
        Try
            Dim chunk As Bitmap = New Bitmap(CropSite.Size.Width, CropSite.Size.Height)
            Using grp = Graphics.FromImage(chunk)
                grp.DrawImage(pctLoad.Image, New Rectangle(0, 0, CropSite.Width, CropSite.Height), CropSite, GraphicsUnit.Pixel)
                pctProccess.Image = chunk
            End Using
            Button2_Click(Nothing, Nothing)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim target As Bitmap = New Bitmap(width:=pctLoad.Image.Width / 2, height:=pctLoad.Image.Height / 2)
        For y = 0 To target.Height - 1
            For x = 0 To target.Width - 1
                Dim isBlack As Boolean = DirectCast(pctLoad.Image, Bitmap).GetPixel(x, y).R > 255 / 2
                isBlack = isBlack Xor DirectCast(pctLoad.Image, Bitmap).GetPixel(x * 2, y).R > 255 / 2
                isBlack = isBlack Xor DirectCast(pctLoad.Image, Bitmap).GetPixel(x, y * 2).R > 255 / 2
                isBlack = isBlack Xor DirectCast(pctLoad.Image, Bitmap).GetPixel(x * 2, y * 2).R > 255 / 2
                target.SetPixel(x, y, IIf(isBlack, Color.White, Color.Black))
            Next
        Next
        pctLoad.Image = target.Clone
        pctProccess.Image = target
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim target As Bitmap = New Bitmap(width:=pctLoad.Image.Width / 2, height:=pctLoad.Image.Height / 2)
        For y = 0 To target.Height - 1
            For x = 0 To target.Width - 1
                Dim isBlack As Boolean = DirectCast(pctLoad.Image, Bitmap).GetPixel(x, y).R > 255 / 2
                isBlack = isBlack And DirectCast(pctLoad.Image, Bitmap).GetPixel(x * 2, y).R > 255 / 2
                isBlack = isBlack And DirectCast(pctLoad.Image, Bitmap).GetPixel(x, y * 2).R > 255 / 2
                isBlack = isBlack And DirectCast(pctLoad.Image, Bitmap).GetPixel(x * 2, y * 2).R > 255 / 2
                target.SetPixel(x, y, IIf(isBlack, Color.White, Color.Black))
            Next
        Next
        pctLoad.Image = target.Clone
        pctProccess.Image = target
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim target As Bitmap = New Bitmap(width:=pctLoad.Image.Width, height:=pctLoad.Image.Height)
        For y = 0 To target.Height - 1
            For x = 0 To target.Width - 1
                Dim isBlack As Boolean = DirectCast(pctLoad.Image, Bitmap).GetPixel(x, y).R > 255 / 2
                target.SetPixel(x, y, IIf(Not isBlack, Color.White, Color.Black))
            Next
        Next
        pctProccess.Image = target
        pctLoad.Image = target.Clone
    End Sub
End Class
